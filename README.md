# Introduction
Prototypes for dockerizing web applications.

* live-version
  * Convenient for local testing of your html/css/js.
  * No Docker involved anywhere.
* nginx
  * An nginx-based Docker image, hosting the SPA.

# References
* https://github.com/HoverBaum/super-simple-dockerized-spa
