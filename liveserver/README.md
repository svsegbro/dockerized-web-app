# Introduction
A simple example illustarting how to run a demo website using *live-server*.

# Getting started
* Make sure you have *nodejs* and *npm* installed
* Initialize a *package.json*
  * Run *npm init*
* Install *live-server*
  * Run *npm install -D live-server*
* Include a start script to package.json: *"start": "live-server --entry-file=index.html"*
* Run *npm start*
